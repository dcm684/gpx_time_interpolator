#GPX Time Interpolator
This script will load the given GPX file, look for GPX trackpoints missing a 
time element and calculate the time based on the average speed travelled 
between the preceding trackpoint with a known time and the next trackpoint 
with a known time.

I created this script to help me correct my GPX tracks where I paused my
watch and then forget to start it back before running/riding again. My workflow 
is as follows:
  1. Load GPX file to be edited into you desired GPX editor
  2. Add missing points to activity
  3. Save updated GPX file
  4. Run this script on the updated file
  
# Usage

`gpx_time_interpolate.py [-h] [-o OUT_FILE] in_file`

**positional arguments:**

  `in_file`      GPX file to process

**optional arguments:**
  
  `-o OUT_FILE`  Destination to save file. If none is given, a new file named
               similar to the in_file will be created, but with _updated
               appended  
   
  `-h`, `--help`   show this help message and exit
