#! /bin/python3

# =============================================================================
# Copyright (c) 2020, Christopher Meyer
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#
# Redistributions of source code must retain the above copyright
# notice, this list of conditions and the following disclaimer.
#
# Redistributions in binary form must reproduce the above copyright
# notice, this list of conditions and the following disclaimer in the
# documentation and/or other materials provided with the distribution.
#
# Neither the name of the project nor the names of its contributors may
# be used to endorse or promote products derived from this software
# without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
# OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
# AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY
# WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
# OF SUCH DAMAGE.
# =============================================================================

# Reads the given GPX file and adds times to points that have position information but are missing times
# There must be a position with time information prior to and after the point(s) missing times. The times
# are based on the average speed from the last point with a known time and next point with known time.

from xml.dom import minidom


def process_gpx_file(filename: str):
    """
    Process the given GPX file adding a calculated time to each of the file's track's trackpoints

    :param filename: GPX file to process
    :return: Tuple
        Bool: True if the XML DOM was modified
        XML DOM of the GPX file
    """
    # Load the GPX file
    # Parse each track, correcting them if necessary
    # Indicate if the GPX was updated and return the GPX data

    import xml.parsers.expat

    try:
        xml_document = minidom.parse(filename)
        xml_document.normalize()
    except xml.parsers.expat.ExpatError:
        # Let the user know what file broke, before exiting with nothing
        print("Malformed XML in \"", filename, "\"")
        return False, None

    # Get the tracks
    gpx_document_element = xml_document.documentElement

    # Process each track in the file
    document_modified = False
    for node_track in gpx_document_element.getElementsByTagName('trk'):
        track_modified = process_gpx_track(xml_document, node_track)

        if track_modified:
            document_modified = True

    return document_modified, xml_document


def process_gpx_track(xml_document: minidom.Document, gpx_node_track: minidom.Element):
    """
    Process the given GPX track, adding estimated times for trackpoints missing their time elements
    :param xml_document: Document containing the track being processed
    :param gpx_node_track: GPX track to be processed
    :return: True if changes were made to gpx_node_track, false otherwise
    """

    # Look for points with missing time fields.
    first_node = True
    previous_trackpoint = None
    nodes_tracked = None  # First node will be the last one with time, the rest will be missing time
    changes_made_to_gpx = False
    current_gap_distance = 0

    for node_trackpoint in gpx_node_track.getElementsByTagName('trkpt'):

        # Check if there is a time element in the trackpoint
        node_time = node_trackpoint.getElementsByTagName('time')

        if len(node_time) == 0:
            if not first_node and previous_trackpoint is not None:
                if nodes_tracked is None:
                    # Capture last point that had a time (node, 0)
                    nodes_tracked = [(previous_trackpoint, 0)]  # Second value is distance

                # Determine the distance from the previous point
                distance_from_last = calculate_distance_between_nodes(nodes_tracked[-1][0], node_trackpoint)

                current_gap_distance += distance_from_last

                nodes_tracked.append((node_trackpoint, distance_from_last))

        else:
            # The previous nodes have been missing time. last_node_with_time
            # would not have been non-None otherwise
            if nodes_tracked is not None:

                changes_made_to_gpx = True

                # Add the current node (with time) to the node list since it is needed to total distance and time
                distance_from_last = calculate_distance_between_nodes(nodes_tracked[-1][0], node_trackpoint)
                nodes_tracked.append((node_trackpoint, distance_from_last))

                current_gap_distance += distance_from_last
                print("Gap without time found: {:.1f} km".format(current_gap_distance))
                current_gap_distance = 0

                # Add the missing points
                add_time_to_missing_trackpoints(xml_document, nodes_tracked)

                # Reset missing node storage containers
                nodes_tracked = None


        # If the first node is missing time nothing can be done until a node with
        # time is found
        if nodes_tracked is None:
            first_node = False

        previous_trackpoint = node_trackpoint

    return changes_made_to_gpx


def add_time_to_missing_trackpoints(xml_document: minidom.Document, nodes_tracked: (minidom.Element, float)):
    """
    Cycles through the nodes missing their time elements and adds them based on the average speed from the
    first to final nodes.

    :param xml_document: XML document containing the given nodes.
    :param nodes_tracked: List of nodes to add time to. First point and final point are expected to have times
        Values are tuples (XML element, distance from the previous point in kilometers)
    :return: None. nodes are directly modified
    """

    # Get total distance travelled
    total_km_of_no_time = sum(node[1] for node in nodes_tracked)

    # Determine time elapsed and then average speed (km/s)
    # Nodes are known to have a time value, so no need for try except
    start_time = nodes_tracked[0][0].getElementsByTagName('time')[0].firstChild.data
    start_time = convert_gpx_time_to_timestamp(start_time)

    end_time = nodes_tracked[-1][0].getElementsByTagName('time')[0].firstChild.data
    end_time = convert_gpx_time_to_timestamp(end_time)

    seconds_elapsed = end_time - start_time
    speed_average = total_km_of_no_time / seconds_elapsed

    # Add time to all of the points without time elements
    previous_point_time = start_time
    for element_without_time, distance in nodes_tracked[1:-1]:
        # Calculate the time
        segment_time = distance / speed_average

        # Add the time element to the XML document
        time_element = xml_document.createElement('time')
        time_in_gpx = convert_timestamp_to_gpx_time(previous_point_time + segment_time)
        time_text = xml_document.createTextNode("{}".format(time_in_gpx))
        time_element.appendChild(time_text)
        element_without_time.appendChild(time_element)

        previous_point_time += segment_time


def convert_gpx_time_to_timestamp(gpx_time: str) -> float:
    """
    Takes a time in the form "Year-Month-Day Hour:Minute:Second" and returns
    in the form of a python timestamp

    :param gpx_time: String representing time, 2020-07-04T12:45:48Z
    :return: Epoch timestamp in seconds
    """

    import time
    import datetime

    converted_time = 0
    try:
        converted_time = time.mktime(datetime.datetime.strptime(gpx_time, '%Y-%m-%dT%H:%M:%SZ').timetuple())
    except ValueError:
        print("Invalid time format, expected '%Y-%m-%dT%H:%M:%SZ'. received {}".format(gpx_time))

    return converted_time


def convert_timestamp_to_gpx_time(timestamp: float) -> str:
    """
    Takes a python timestamp and returns it as GPX time in the form "Year-Month-Day Hour:Minute:Second"

    :param timestamp: Python timestamp in seconds to create GPX timestamp from
    :return: GPX time stamp
    """
    import time

    out_time = time.strftime('%Y-%m-%dT%H:%M:%SZ', time.localtime(timestamp))

    return out_time


def calculate_distance_between_nodes(point_a: minidom.Element, point_b: minidom.Element) -> float:
    """
    Find the distance between two GPX trackpoint nodes

    :param point_a: GPX trackpoint element to calculate distance from
    :param point_b: GPX trackpoint element to calculate distance to
    :return: Distance between points in kilometers
    """

    # Get the latitude and longitude from both nodes. If either is missing one, fail out.
    distance = -1
    try:
        lat_a = float(point_a.getAttribute('lat'))
        lon_a = float(point_a.getAttribute('lon'))
        lat_b = float(point_b.getAttribute('lat'))
        lon_b = float(point_b.getAttribute('lon'))

    except IndexError:
        pass

    else:
        distance = great_circle_distance(lat_a, lon_a, lat_b, lon_b)

    return distance


def great_circle_distance(lat1: float, lon1: float, lat2: float, lon2: float) -> float:
    """
    Finds the distance between two points taking the curvature of the Earth
    into consideration. Returns the value in kilometers

    :param lat1: Latitude of point 1
    :param lon1: Longitude of point 1
    :param lat2: Latitude of point 2
    :param lon2: Longitude of point 2
    :return: Distance in kilometers between points
    """
    import math

    _earth_radius = 6378.7  # Average radius of the Earth in km

    rads_lat1 = math.radians(lat1)
    rads_lat2 = math.radians(lat2)

    try:
        retval_a = math.acos(math.sin(rads_lat1) * math.sin(rads_lat2) +
                             math.cos(rads_lat1) * math.cos(rads_lat2) *
                             math.cos(math.radians(lon2-lon1))
                             ) * _earth_radius
    except ValueError:
        # If both points are the same, the distance is 0.
        if(rads_lat1 == rads_lat2) and (lon1 == lon2):
            retval_a = 0

    # More accurate means of determining distance, but accuracy is not
    # worth the increase in computation time (think less than 1 mm)
    # ret_valB = 2 * \
    #     math.asin(math.sqrt((math.sin((rads_lat2 - rads_lat1)/2)) ** 2 +
    #     math.cos(rads_lat1) * math.cos(rads_lat2) *
    #         (math.sin(math.radians(lon2-lon1)/2)) ** 2)) * _earth_radius
    # print("A: {:.4f} km B: {:.4f} km -> Delta: {:.10f} m".format(
    #     retval_a, ret_valB, math.fabs(retval_a - ret_valB) * 1000))

    return retval_a


def update_gpx_file(in_filename: str, out_filename: str):
    """
    Process the given GPX file adding a calculated time to each of the file's track's trackpoints, writing the result
    to the given file

    :param in_filename: GPX file to read
    :param out_filename: GPX file to write to. Will be overwritten without prompt
    :return: True if a file was written
    """
    changed, gpx_file_document = process_gpx_file(in_filename)

    # If any points with missing times were found and added, output the data to the given file
    if changed:
        with open(out_filename, 'w') as out_gpx_writer:
            gpx_file_document.writexml(out_gpx_writer)

    return changed


if __name__ == "__main__":

    import sys
    import os

    import argparse

    out_file_suffix = '_updated'

    parser = argparse.ArgumentParser(
        description="Processes a GPX file adding estimated times to trackpoints with positions but no times")
    parser.add_argument(dest='in_file',
                        help="GPX file to process")
    parser.add_argument('-o',
                        dest='out_file',
                        help="Destination to save file. If none is given, a new file "
                             "named similar to the in_file will be created, but "
                             "with {} appended".format(out_file_suffix))

    args = parser.parse_args()

    if not os.path.exists(args.in_file):
        # Verify that the input file exists before continuing
        print("{} does not exist".format(args.in_file))
        exit()

    if args.out_file is None:
        # if no out file is provided, create one based on the in file and the default suffix
        args.out_file = "{}{}.gpx".format(os.path.splitext(args.in_file)[0], out_file_suffix)

    print("Processing {} -> {}".format(args.in_file, args.out_file))

    update_gpx_file(args.in_file, args.out_file)
